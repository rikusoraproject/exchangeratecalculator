<?php

$msg_ini=parse_ini_file("message.ini",true);
$currency_ini=parse_ini_file("currency.ini",true);

$_l = "en";

function setlang($l) {
    global $_l;
    $_l = $l;
}

function _CIMG($ccy) {
    global $currency_ini;
    if(isset($currency_ini['img']) && isset($currency_ini['img'][$ccy])) {
        return $currency_ini['img'][$ccy];
    }
    return "";
}

function _C($ccy) {
    global $_l;
    global $currency_ini;

    if(isset($currency_ini[$_l]) && isset($currency_ini[$_l][$ccy])) {
        return $currency_ini[$_l][$ccy];
    }
    return "_C not found";
}

function _C1($ccy) {
    return _C($ccy).'('.$ccy.')';    
}

function _C2($ccy) {
    return $ccy.'('._C($ccy).')';    
}

function _T($msgno) {
    global $_l;
    global $msg_ini;
    
    if(isset($msg_ini[$_l]) && isset($msg_ini[$_l][$msgno])) {
        return $msg_ini[$_l][$msgno];
    }
    return "_T not found";
}
?>