<?php
  set_error_handler( 'error_handler', E_ALL );
  function error_handler($errno, $errstr, $errfile, $errline, $errcontext) {
    header('Location: /');
    exit;
  }  

  if($_SERVER['REQUEST_METHOD']==='POST') {
    foreach (['lang', 'amount', 'rateTo', 'rateFrom', 'tz'] as $v) {
      $$v = (string)filter_input(INPUT_POST, $v);
    }
    setcookie('lang', $lang, strtotime('+30 days'), '/');
    setcookie('rateTo', $rateTo, strtotime('+30 days'), '/');
    setcookie('rateFrom', $rateFrom, strtotime('+30 days'), '/');
    setcookie('tz', $tz, strtotime('+30 days'), '/');

    $sv = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];

    header("AMP-Access-Control-Allow-Source-Origin:$sv");

    header("AMP-Redirect-To: $sv/$lang/$rateTo/$rateFrom/$amount");
    header("Access-Control-Expose-Headers: AMP-Redirect-To, AMP-Access-Control-Allow-Source-Origin");
    exit;
  }
?>