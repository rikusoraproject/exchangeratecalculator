<?php
    require_once('../rate.php');

    define('BASEURL', 'https://calc.jpn.org/');
    define('SITEMAPINDEX', 'sitemap-index.xml');

    //サイトマップインデックス
    if (file_exists(SITEMAPINDEX)) {
        unlink(SITEMAPINDEX);
    }
    $sh = '<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;
    file_put_contents(SITEMAPINDEX, $sh, FILE_APPEND);

    createSiteMapTxt();

    $sf = '</sitemapindex>';
    file_put_contents(SITEMAPINDEX, $sf, FILE_APPEND);

    function createSiteMapTxt() {
        global $rate;

        $langs=array('ja','en','ko','zh','es');
        $fileindex=1;
        $counter=1;
        
        $url = '';
        foreach($langs as $lang) {
            foreach ($rate as $key => $value) {
                $f = substr($key, 0, 3);
                $t = substr($key, 3, 3);
                $locbase = '<loc>'.BASEURL . $lang .'/' . $f . '/' . $t . '/';
                $ftbase = $f.'/'.$t.'/';
                for ($i=1; $i<=10000; $i++, $counter++) {
                    $loc = $locbase . $i.'</loc>';
                    $linkja = '<xhtml:link rel="alternate" hreflang="ja" href="'.BASEURL.'ja/'.$ftbase.$i.'"/>';
                    $linken = '<xhtml:link rel="alternate" hreflang="en" href="'.BASEURL.'en/'.$ftbase.$i.'"/>';
                    $linkko = '<xhtml:link rel="alternate" hreflang="ko" href="'.BASEURL.'ko/'.$ftbase.$i.'"/>';
                    $linkzh = '<xhtml:link rel="alternate" hreflang="zh" href="'.BASEURL.'zh/'.$ftbase.$i.'"/>';
                    $linkes = '<xhtml:link rel="alternate" hreflang="es" href="'.BASEURL.'es/'.$ftbase.$i.'"/>';
                    $url = $url. '<url>'.$loc.$linkja.$linken.$linkko.$linkzh.$linkes.'</url>'.PHP_EOL;
                    if($counter==10000) {
                        $fname='sitemap'.sprintf('%05d', $fileindex).'.xml';

                        // サイトマップインデックス出力
                        $si = "<sitemap><loc>".BASEURL."$fname</loc></sitemap>".PHP_EOL; 
                        file_put_contents(SITEMAPINDEX, $si, FILE_APPEND);

                        // サイトマップ出力
                        if (file_exists($fname)) {
                            unlink($fname);
                        }
                        $head = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;
                        file_put_contents($fname, $head, FILE_APPEND);
                        file_put_contents($fname, $url, FILE_APPEND);
                        $foot = '</urlset>'.PHP_EOL;
                        file_put_contents($fname, $foot, FILE_APPEND);

                        $url='';    
                        $counter=0;
                        $fileindex++;
                    }
                }
            }
        }

        $fname='sitemap'.sprintf('%05d', $fileindex).'.xml';

        // サイトマップインデックス
        $si = "<sitemap><loc>".BASEURL."$fname</loc></sitemap>".PHP_EOL; 
        file_put_contents(SITEMAPINDEX, $si, FILE_APPEND);
        if (file_exists($fname)) {
            unlink($fname);
        }
        file_put_contents($fname, $url, FILE_APPEND);

    }
?>
