<?php
$ratephp='../rate.php';

$url = "https://spreadsheets.google.com/feeds/list/1ziofZ3CWoVb5BTaaiYA-7U5RgRH-nFJJRq_wAkk-Odk/1/public/values?alt=json";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$html = curl_exec($ch);
curl_close($ch);
$json_decode = json_decode($html);
$data = $json_decode->feed->entry;

if (file_exists($ratephp)) {
    unlink($ratephp);
}

$timestamp = gmdate("c");
$sh='<?php'.PHP_EOL.' $rate_update=\''.$timestamp.'\';'.PHP_EOL.' $rate =array('.PHP_EOL; 
file_put_contents($ratephp, $sh, FILE_APPEND);


$exclude = array();
foreach ($data as $line) {
    if ($line->{'gsx$del'}->{'$t'}=='1' || $line->{'gsx$del'}->{'$t'}=='2') {
        $exclude[] = $line->{'gsx$currencycode'}->{'$t'};
    }
}

$output='';
foreach ($data as $line) {
    if($line->{'gsx$del'}->{'$t'}!='1' && $line->{'gsx$del'}->{'$t'}!='2'){
        $lineary = explode(',', $line->{'content'}->{'$t'});
        $from = array_shift($lineary);
        $from = strtoupper(trim(explode(':', $from)[1]));
        foreach ($lineary as $value) {
            list($to, $rate)=explode(':', $value);
            $to=trim(strtoupper($to));
            if (!in_array($to, $exclude)) {
                $rate=trim($rate);
                if ($rate!='#N/A') {
                    $output=$output.'    \''.$from.$to.'\'=>'.$rate.','.PHP_EOL;
                }
            }
        }
    } 
}   
file_put_contents($ratephp, $output, FILE_APPEND);
file_put_contents($ratephp, ');'.PHP_EOL.'?>', FILE_APPEND);

?>
