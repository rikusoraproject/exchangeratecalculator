<?php

$messagephp='../message.ini';

if (file_exists($messagephp)) {
    unlink($messagephp);
}

    $url = "https://spreadsheets.google.com/feeds/list/1ziofZ3CWoVb5BTaaiYA-7U5RgRH-nFJJRq_wAkk-Odk/8/public/values?alt=json";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $html = curl_exec($ch);
    curl_close($ch);
    $json_decode = json_decode($html);
    $data = $json_decode->feed->entry;

    $output = array();

    foreach($data as $line) {
        $msgno = $line->{'gsx$msgno'}->{'$t'};
        $output['ja'][$msgno]=$line->{'gsx$ja'}->{'$t'};
        $output['en'][$msgno]=$line->{'gsx$en'}->{'$t'};
        $output['ko'][$msgno]=$line->{'gsx$ko'}->{'$t'};
        $output['zh'][$msgno]=$line->{'gsx$zh'}->{'$t'};
        $output['es'][$msgno]=$line->{'gsx$es'}->{'$t'};
    }

    foreach($output as $lang=>$row) {
        file_put_contents($messagephp, '['.$lang.']'.PHP_EOL, FILE_APPEND);
        foreach($row as $msgno=>$msg) {
            file_put_contents($messagephp, $msgno.'="'.$msg.'"'.PHP_EOL, FILE_APPEND);
        }
    }

?>
