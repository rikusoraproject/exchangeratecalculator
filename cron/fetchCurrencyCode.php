<?php

$currencyphp='../currency.ini';

if (file_exists($currencyphp)) {
    unlink($currencyphp);
}

$langs = array('ja','en', 'ko', 'zh', 'es', 'img');
foreach($langs as $i=>$lang) {
    $url = "https://spreadsheets.google.com/feeds/list/1ziofZ3CWoVb5BTaaiYA-7U5RgRH-nFJJRq_wAkk-Odk/".($i+2)."/public/values?alt=json";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $html = curl_exec($ch);
    curl_close($ch);
    $json_decode = json_decode($html);
    $data = $json_decode->feed->entry;

    file_put_contents($currencyphp, '['.$lang.']'.PHP_EOL, FILE_APPEND);
    $output='';
    foreach($data as $line) {
        $code = trim(explode(':',explode(',',$line->{'content'}->{'$t'})[1])[1]);
        $code = substr($code,0,3);  // XXX[7]などがあるため。
        $currency = trim(explode(':',explode(',',$line->{'content'}->{'$t'})[0])[1]);
        $currency = preg_replace('/\[.+\]/', '', $currency);
        
        $output=$output.$code.'="'.$currency.'"'.PHP_EOL;
    }
    file_put_contents($currencyphp, $output, FILE_APPEND);
    file_put_contents($currencyphp, PHP_EOL, FILE_APPEND);
}

?>
